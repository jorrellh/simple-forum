class AddColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :title, :text
    add_column :posts, :content, :text
    add_reference :posts, :user

    add_column :replies, :content, :text
    add_reference :replies, :user
    add_reference :replies, :post
  end
end
