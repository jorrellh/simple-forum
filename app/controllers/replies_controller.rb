class RepliesController < ApplicationController
  def create
    Reply.create(reply_params.merge(user_id: current_user.id))
  end
  def reply_params
    params.require(:reply).permit(:post_id, :content)
  end
end
