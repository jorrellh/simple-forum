class PostsController < ApplicationController

  def index
    @posts = Post.all.order(created_at: :desc)
  end

  def show
    @post = Post.find(params[:id])
    @reply = Reply.new
  end

  def new
    @post = Post.new
  end

  def create
    Post.create(post_params.merge(user_id: current_user.id))
    redirect_to posts_url
  end

  def post_params
    params.require(:post).permit(:title, :content)
  end
end
